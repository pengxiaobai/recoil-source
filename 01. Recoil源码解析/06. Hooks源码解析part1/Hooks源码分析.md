# Hooks源码分析

- useRecoilState
- useRecoilValue
- useSetRecoilState



## useRecoilState

对应的源码如下：

```ts
// useRecoilState 接收一个参数，该参数是一个 RecoilState 类型的数据
// [T, SetterOrUpdater<T>] 是该 Hook 的返回值类型
function useRecoilState<T>(recoilState: RecoilState<T>,): [T, SetterOrUpdater<T>] {
  return [useRecoilValue(recoilState), useSetRecoilState(recoilState)];
}
```

整个 useRecoilState 这个 Hook 的源码是非常简单的，内部使用的就是 useRecoilValue 以及 useSetRecoilState 这两个 Hook。



## useRecoilValue

对应的源码如下：

```ts
function useRecoilValue<T>(recoilValue: RecoilValue<T>): T {
  // 首先通过 useStoreRef 这个 Hook 拿到一个 ref，该 ref 是和 store 相关的
  const storeRef = useStoreRef();
  // 调用 useRecoilValueLoadable 这个 Hook，该 Hook 接收一个 recoilValue 的数据
  // 返回一个 loadable 类型的数据
  const loadable = useRecoilValueLoadable(recoilValue);
  // handleLoadable 一看就是处理上一步所得到的 loadable 类型的数据
  return handleLoadable(loadable, recoilValue, storeRef);
}
```

useRecoilValue 这个 Hook 的核心作用是获取 Recoil 状态的值，它首先会获取 Recoil 全局状态存储的引用和 recoilValue 的 loadable 类型的值，然后调用 handleLoadable 来对得到的 loadable 类型的值进行处理并返回。

- useStoreRef
- useRecoilValueLoadable
- handleLoadable



### useStoreRef

useStoreRef 这个 Hook 位于 core/Recoil_RecoilRoot，对应的源码如下：

```ts
// 这里核心就是调用了 React 的 useContext 这个 Hook
// 也就是说，Recoil 背后实际上还是使用 React 的 Context API 来管理的状态
const useStoreRef = (): StoreRef => useContext(AppContext);
```



### useRecoilValueLoadable

得到一个 loadable 类型的数据：

```ts
function useRecoilValueLoadable<T>(recoilValue: RecoilValue<T>,): Loadable<T> {
	// 首先检查 recoilValuesUsed.current（这是一个 Set 集合）是否包含 recoilValue 键值
  // 如果不包含，会调用 setByAddingToSet 方法，然后更新 recoilValuesUsed.current
  if (!recoilValuesUsed.current.has(recoilValue.key)) {
    recoilValuesUsed.current = setByAddingToSet(
      recoilValuesUsed.current,
      recoilValue.key,
    );
  }
  
  // 获取 Recoil 的全局状态存储
  // 全局状态存储包含了所有的 Recoil 状态的值
  const storeState = storeRef.current.getState();
  
  // 最后就是调用 getRecoilValueAsLoadable
  // 该方法会得到一个 loadable 类型的数据
  return getRecoilValueAsLoadable(
    storeRef.current,
    recoilValue,
    reactMode().early
      ? storeState.nextTree ?? storeState.currentTree
      : storeState.currentTree,
  );
}
```



### handleLoadable

对上一步所得到的 loadable 类型的数据进行一个处理。对应的源码如下：

```ts
function handleLoadable<T>(loadable: Loadable<T>,recoilValue: RecoilValue<T>,storeRef: StoreRef,): T {
  // 这里面的逻辑是比较清晰
  // 根据 loadable 类型值的不同状态来做处理
  // 1. hasValue（已经有值）2. loading（正在加载） 3. hasError（有错误）
  
  // 下面就是针对不同的状态来做处理
  if (loadable.state === 'hasValue') {
    // 直接返回响应的值
    return loadable.contents;
  } else if (loadable.state === 'loading') {
    // 如果是 loading 状态，创建一个新的 promise
    const promise = new Promise(resolve => {
      const suspendedComponentResolvers =
        storeRef.current.getState().suspendedComponentResolvers;
      suspendedComponentResolvers.add(resolve);

      if (isSSR && isPromise(loadable.contents)) {
        loadable.contents.finally(() => {
          suspendedComponentResolvers.delete(resolve);
        });
      }
    });
    throw promise;
  } else if (loadable.state === 'hasError') {
    // 如果是有错误，返回错误信息
    throw loadable.contents;
  } else {
    // 不是上面三个状态的其中一个，那么说明也是有问题的，因此抛出一个一个错误
    throw err(`Invalid value of loadable atom "${recoilValue.key}"`);
  }
}
```

总结一下，handleLoadable 方法实际上就是根据传入的 loadable 类型的数据的不同状态，来做不一样的处理。



## useSetRecoilState


