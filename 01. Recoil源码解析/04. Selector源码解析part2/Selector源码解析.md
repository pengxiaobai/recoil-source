# Selector源码解析

首先回顾一下 selector 的基础使用方法，如下：

```ts
const exampleSelector = selector({
  key: 'example',
  get: ({get}) => {
    const a = get(atomA);
    const b = get(atomB);
    return a + b;
  },
});
```



## 整体结构

整个 selector 的源码非常的庞大，但是整体的结构是比较清晰的：

```ts
function selector<T>(
  options: ReadOnlySelectorOptions<T> | ReadWriteSelectorOptions<T>,
): RecoilValue<T> {
  let recoilValue: ?RecoilValue<T> = null;

  const {key, get, cachePolicy_UNSTABLE: cachePolicy} = options;
  const set = options.set != null ? options.set : undefined; // flow

  // This is every discovered dependency across all executions
  const discoveredDependencyNodeKeys = new Set<NodeKey>();

  const cache: TreeCacheImplementation<Loadable<T>> = treeCacheFromPolicy(
    cachePolicy ?? {
      equality: 'reference',
      eviction: 'keep-all',
    },
    key,
  );

  const retainedBy = retainedByOptionWithDefault(options.retainedBy_UNSTABLE);

  const executionInfoMap: Map<Store, ExecutionInfo<T>> = new Map();
  let liveStoresCount = 0;
    
  // 声明一堆内部方法...
    
  if (set != null) {
    /**
     * ES5 strict mode prohibits defining non-top-level function declarations,
     * so don't use function declaration syntax here
     */
    const selectorSet = (
      store: Store,
      state: TreeState,
      newValue: T | DefaultValue,
    ): AtomWrites => {
      // ...
    };

    return (recoilValue = registerNode<T>({
      key,
      nodeType: 'selector',
      peek: selectorPeek,
      get: selectorGet,
      set: selectorSet,
      init: selectorInit,
      invalidate: invalidateSelector,
      clearCache: clearSelectorCache,
      shouldDeleteConfigOnRelease: selectorShouldDeleteConfigOnRelease,
      dangerouslyAllowMutability: options.dangerouslyAllowMutability,
      shouldRestoreFromSnapshots: false,
      retainedBy,
    }));
  } else {
    return (recoilValue = registerNode<T>({
      key,
      nodeType: 'selector',
      peek: selectorPeek,
      get: selectorGet,
      init: selectorInit,
      invalidate: invalidateSelector,
      clearCache: clearSelectorCache,
      shouldDeleteConfigOnRelease: selectorShouldDeleteConfigOnRelease,
      dangerouslyAllowMutability: options.dangerouslyAllowMutability,
      shouldRestoreFromSnapshots: false,
      retainedBy,
    }));
  }
}
```

接下来我们一个部分一个部分来看。

```ts
// 首先是开头部分的代码

// 首先这里声明了一个变量，该变量的初始值为 null，回头会存放 recoilVaule 类型的值
let recoilValue: ?RecoilValue<T> = null;
// 从开发者传入的配置对象中解构出：
// key：selector 的唯一标识
// get：一个 getter，用于计算 selctor 的值
// cachePolicy：用于决定如何缓存此 selector 的值
const {key, get, cachePolicy_UNSTABLE: cachePolicy} = options;
// 开发者如果传入了 set，那么将 set 赋值给这里的 set 变量，否则就为一个 undefined
const set = options.set != null ? options.set : undefined; // flow

// This is every discovered dependency across all executions
// discoveredDependencyNodeKeys 是一个 set 类型的数据结构
// 用于存储所有执行过程中发现的依赖项的 key
const discoveredDependencyNodeKeys = new Set<NodeKey>();

// 调用 treeCacheFromPolicy 方法根据所提供的缓存策略以及 key 来创建一个缓存
// 如果没有提供缓存策略，那么会使用一个默认的策略
const cache: TreeCacheImplementation<Loadable<T>> = treeCacheFromPolicy(
  cachePolicy ?? {
    equality: 'reference',
    eviction: 'keep-all',
  },
  key,
);

// 设置 selector 的保留策略
const retainedBy = retainedByOptionWithDefault(options.retainedBy_UNSTABLE);

// executionInfoMap 是一个 map 类型的数据结构，主要用于存储每个 store 的执行信息
const executionInfoMap: Map<Store, ExecutionInfo<T>> = new Map();
// 这是一个计数器，用于记录当前活跃的 store 的数量
let liveStoresCount = 0;
```

总结一下，这段代码的作用是根据提供的 options 创建与该 selector 相关的各个数据结构，包括缓存、依赖项集合、执行信息映射之类的。



接下来我们来看一下返回值。对应的代码如下：

```ts
if (set != null) {
    /**
     * ES5 strict mode prohibits defining non-top-level function declarations,
     * so don't use function declaration syntax here
     */
    const selectorSet = (
      store: Store,
      state: TreeState,
      newValue: T | DefaultValue,
    ): AtomWrites => {
      // ...
    };

    return (recoilValue = registerNode<T>({
      key,
      nodeType: 'selector',
      peek: selectorPeek,
      get: selectorGet,
      set: selectorSet,
      init: selectorInit,
      invalidate: invalidateSelector,
      clearCache: clearSelectorCache,
      shouldDeleteConfigOnRelease: selectorShouldDeleteConfigOnRelease,
      dangerouslyAllowMutability: options.dangerouslyAllowMutability,
      shouldRestoreFromSnapshots: false,
      retainedBy,
    }));
  } else {
    return (recoilValue = registerNode<T>({
      key,
      nodeType: 'selector',
      peek: selectorPeek,
      get: selectorGet,
      init: selectorInit,
      invalidate: invalidateSelector,
      clearCache: clearSelectorCache,
      shouldDeleteConfigOnRelease: selectorShouldDeleteConfigOnRelease,
      dangerouslyAllowMutability: options.dangerouslyAllowMutability,
      shouldRestoreFromSnapshots: false,
      retainedBy,
    }));
  }
```

主要就是判断是否有 set，不管有没有 set，最终调用的都是 registerNode 方法。nodeType 对应的为 selector。

如果 set 不为 null，那么在调用 registerNode 方法的时候，会设置 set 对应 selectorSet，说明会创建一个可读可写类型的 selector。

如果 set 为 null，那么会创建一个只读类型的 selector。



接下来看一下 selector 里面对应的内部方法，这里主要看一下：

- selectorPeek
- selectorGet
- selectorSet
- selectorInit
- invalidateSelector



## selectorPeek方法

该方法用于预览当前 selector 的值。对应的源码如下：

```ts
// 接收两个参数
// store：状态仓库
// state：状态树
function selectorPeek(store: Store, state: TreeState): ?Loadable<T> {
  // 首先第一步，通过 key 从 state.atomValues 里面获取对应的 selector 值
  // Loadable 是 Recoil 里面所定义的一种值的类型
  // 这种值会有三个状态：1.确认已经有值 2. 可能正在加载 3. 加载失败
  const cachedLoadable = state.atomValues.get(key);
  if (cachedLoadable != null) {
    // 如果能够从 state.atomValues 获取到对应的值，那么就直接返回这个值
    return cachedLoadable;
  }

	// 如果没有从 state.atomValues 里面获取到对应的值
  // 那么就从 cache.get 方法里面去拿
  return cache.get(nodeKey => {
    invariant(typeof nodeKey === 'string', 'Cache nodeKey is type string');
    return peekNodeLoadable<mixed>(store, state, nodeKey)?.contents;
  });
}
```

总结一下，selectorPeek 用于获取指定的 selector 的当前值。首先会尝试从状态树的缓存中去获取，如果没有获取到，那么就会尝试从 cache 中去获取。这是内部的一种优化手段，避免了去做不必要的重新计算。



## selectorGet方法

同样是获取 selector 的当前值，对应的源码如下：

```ts
function selectorGet(store: Store, state: TreeState): Loadable<T> {
  if (store.skipCircularDependencyDetection_DANGEROUS === true) {
    // 如果进入此分支，那么说明关闭了循环依赖的检测
    // 那么直接调用 getSelectorLoadableAndUpdateDeps 方法来获取 selector 的值
    return getSelectorLoadableAndUpdateDeps(store, state);
  }
  return detectCircularDependencies(() =>
    getSelectorLoadableAndUpdateDeps(store, state),
  );
}
```

这段代码的核心，就是通过 getSelectorLoadableAndUpdateDeps 方法去获取 selector 的值。只不过在获取值之前，会做一个循环依赖的检测。通过 detectCircularDependencies 方法来检测循环依赖，但是存在一种情况，可能循环依赖的检测是关闭了的，如果关闭了循环依赖检测，那么会进入上面的 if，直接调用 getSelectorLoadableAndUpdateDeps 方法去获取 selector 的值。如果没有关闭那么就是通过 detectCircularDependencies 去做循环依赖的检测。



这里要说一下，关于 selectorPeek 和 selectorGet 都是获取 selector 对应的值，但是它们的用途和行为是略有不同的：

- selectorPeek 仅仅是从缓存或者状态树中获取 selector 的值，**不会触发重新计算**，所以它仅仅是用来预览当前的 selector 的值，另外如果没有缓存的值，最终得到的 null。
- selectorGet 在获取值的时候，如果发现值已经失效（例如依赖的 atom 或者 selector 发生了改变），那么会触发它的重新计算。



## selectorInit方法

该方法一看就知道，是用来初始化 selector 的。

对应的源码如下：

```ts
function selectorInit(store: Store): () => void {
  // 将对应的 key 添加到 selectors 的集合当中
  store.getState().knownSelectors.add(key);
  // liveStoresCount 是一个计数器，用于跟踪当前有多少个活跃的 store
  liveStoresCount++;
  // 返回的是一个清理函数，该清理函数被调用的时候，会减少活跃 store 的数量
  return () => {
    liveStoresCount--;
  };
}
```

总结一下，selectorInit 方法作用是初始化 selector，它会将 selector 的 key 添加到 selectors 的集合中，并增加活跃 store 的数量。当 selector 不再需要的时候，可以调用返回的清理函数，来减少活跃的 store 的数量。



## invalidateSelector方法

用于让一个特定的 selector 失效。

```ts
function invalidateSelector(state: TreeState) {
  // 就是从 state.atomValues 删除对应 key 的 selector
  state.atomValues.delete(key);
}
```


