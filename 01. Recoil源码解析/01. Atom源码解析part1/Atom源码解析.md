# Atom源码解析

回顾之前学习的 Recoil，整个 Recoil 有两个核心概念：

- atom
- selector

还有三个常用的 Hook：

- useRecoilState
- useRecoilValue
- useSetRecoilState

这门课我们主要也就是针对两个核心概念以及三个 Hook 进行一个源码解析。

因为整个 Recoil 的源码量很多，而且源码本身是使用 Flow.js 来编写的，整体源码的可阅读性其实比较差的，所以我们即便是针对 atom 和 selector 核心概念进行源码解析，也只对里面几个比较核心的方法进行拆解。



首先回顾一下 Atom 的用法：

```ts
atom({
  key: 'xxx',
  default: xxx
})
```



## atom方法

atom 方法的源码如下：

```ts
function atom<T>(options: AtomOptions<T>): RecoilState<T> {
  // 将 options 对象里面的所有属性复制到 restOptions 里面
  const {...restOptions} = options;
  // 检查 options 里面是否有默认值，如果有默认值，就将默认值赋值给 optionsDefault
  // 否则的话，optionsDefault 是一个新的未决的 Promise
  const optionsDefault: RecoilValue<T> | Promise<T> | Loadable<T> | WrappedValue<T> | T =
    'default' in options
      ?
        options.default
      : new Promise(() => {});

  // 接下来判断 optionsDefault 是否是一个 RecoilValue 的值
  if (isRecoilValue(optionsDefault)
  ) {
    // 如果是 RecoilValue，进入此分支，调用 atomWithFallback 方法
    return atomWithFallback<T>({
      ...restOptions,
      default: optionsDefault,
    });
  } else {
    // 否则的话，调用 baseAtom 方法
    return baseAtom<T>({...restOptions, default: optionsDefault});
  }
}
```

atom 方法返回一个 RecoilState 类型的值。对应的类型相关信息的源码如下：

```ts
class AbstractRecoilValue<+T> {
  key: NodeKey;
  constructor(newKey: NodeKey) {
    this.key = newKey;
  }
  toJSON(): {key: string} {
    return {key: this.key};
  }
}

class RecoilState<T> extends AbstractRecoilValue<T> {}

class RecoilValueReadOnly<+T> extends AbstractRecoilValue<T> {}

export type RecoilValue<T> = RecoilValueReadOnly<T> | RecoilState<T>;
```

RecoilState 和 RecoilValueReadOnly 都继承了 AbstractRecoilValue。

还有一个类型 RecoilValue，该类型就是 RecoilState 和 RecoilValueReadOnly 的联合类型。



## atomWithFallback 方法

atomWithFallback 方法的源码如下：

```ts
function atomWithFallback<T>(options: AtomWithFallbackOptions<T>,): RecoilState<T> {
  // 整个 atomWithFallback 的核心逻辑可以分为两大块
  
  // 第一部分：再次调用 atom 方法
  // 这里再次调用 atom 方法会不会形成一个无限递归 ？
  // 答案是不会，因为第二次调用 atom 方法的时候，default 设置为了 DEFAULT_VALUE
  // 最终 base 对应的值其实就是 baseAtom 的返回值
  const base = atom<T | DefaultValue>({
    ...options,
    default: DEFAULT_VALUE,
    persistence_UNSTABLE:
      options.persistence_UNSTABLE === undefined
        ? undefined
        : {
            ...options.persistence_UNSTABLE,
            validator: (storedValue: mixed) =>
              storedValue instanceof DefaultValue
                ? storedValue
                : nullthrows(options.persistence_UNSTABLE).validator(
                    storedValue,
                    DEFAULT_VALUE,
                  ),
          },
    effects: (options.effects: any),
    effects_UNSTABLE: (options.effects_UNSTABLE: any),
  });

  // 第二部分：根据上一步所得到的 baseAtom，创建一个 selector，并返回外部
  const sel = selector<T>({
    key: `${options.key}__withFallback`,
    get: ({get}) => {
      const baseValue = get(base);
      return baseValue instanceof DefaultValue ? options.default : baseValue;
    },
    set: ({set}, newValue) => set(base, newValue),
    cachePolicy_UNSTABLE: {
      eviction: 'most-recent',
    },
    dangerouslyAllowMutability: options.dangerouslyAllowMutability,
  });
  setConfigDeletionHandler(sel.key, getConfigDeletionHandler(options.key));
  return sel;
}
```



## baseAtom方法


