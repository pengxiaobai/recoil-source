# Selector源码解析

首先回顾一下 selector 的基础使用方法，如下：

```ts
const exampleSelector = selector({
  key: 'example',
  get: ({get}) => {
    const a = get(atomA);
    const b = get(atomB);
    return a + b;
  },
});
```



## 整体结构

整个 selector 的源码非常的庞大，但是整体的结构是比较清晰的：

```ts
function selector<T>(
  options: ReadOnlySelectorOptions<T> | ReadWriteSelectorOptions<T>,
): RecoilValue<T> {
  let recoilValue: ?RecoilValue<T> = null;

  const {key, get, cachePolicy_UNSTABLE: cachePolicy} = options;
  const set = options.set != null ? options.set : undefined; // flow

  // This is every discovered dependency across all executions
  const discoveredDependencyNodeKeys = new Set<NodeKey>();

  const cache: TreeCacheImplementation<Loadable<T>> = treeCacheFromPolicy(
    cachePolicy ?? {
      equality: 'reference',
      eviction: 'keep-all',
    },
    key,
  );

  const retainedBy = retainedByOptionWithDefault(options.retainedBy_UNSTABLE);

  const executionInfoMap: Map<Store, ExecutionInfo<T>> = new Map();
  let liveStoresCount = 0;
    
  // 声明一堆内部方法...
    
  if (set != null) {
    /**
     * ES5 strict mode prohibits defining non-top-level function declarations,
     * so don't use function declaration syntax here
     */
    const selectorSet = (
      store: Store,
      state: TreeState,
      newValue: T | DefaultValue,
    ): AtomWrites => {
      // ...
    };

    return (recoilValue = registerNode<T>({
      key,
      nodeType: 'selector',
      peek: selectorPeek,
      get: selectorGet,
      set: selectorSet,
      init: selectorInit,
      invalidate: invalidateSelector,
      clearCache: clearSelectorCache,
      shouldDeleteConfigOnRelease: selectorShouldDeleteConfigOnRelease,
      dangerouslyAllowMutability: options.dangerouslyAllowMutability,
      shouldRestoreFromSnapshots: false,
      retainedBy,
    }));
  } else {
    return (recoilValue = registerNode<T>({
      key,
      nodeType: 'selector',
      peek: selectorPeek,
      get: selectorGet,
      init: selectorInit,
      invalidate: invalidateSelector,
      clearCache: clearSelectorCache,
      shouldDeleteConfigOnRelease: selectorShouldDeleteConfigOnRelease,
      dangerouslyAllowMutability: options.dangerouslyAllowMutability,
      shouldRestoreFromSnapshots: false,
      retainedBy,
    }));
  }
}
```

接下来我们一个部分一个部分来看。

```ts
// 首先是开头部分的代码

// 首先这里声明了一个变量，该变量的初始值为 null，回头会存放 recoilVaule 类型的值
let recoilValue: ?RecoilValue<T> = null;
// 从开发者传入的配置对象中解构出：
// key：selector 的唯一标识
// get：一个 getter，用于计算 selctor 的值
// cachePolicy：用于决定如何缓存此 selector 的值
const {key, get, cachePolicy_UNSTABLE: cachePolicy} = options;
// 开发者如果传入了 set，那么将 set 赋值给这里的 set 变量，否则就为一个 undefined
const set = options.set != null ? options.set : undefined; // flow

// This is every discovered dependency across all executions
// discoveredDependencyNodeKeys 是一个 set 类型的数据结构
// 用于存储所有执行过程中发现的依赖项的 key
const discoveredDependencyNodeKeys = new Set<NodeKey>();

// 调用 treeCacheFromPolicy 方法根据所提供的缓存策略以及 key 来创建一个缓存
// 如果没有提供缓存策略，那么会使用一个默认的策略
const cache: TreeCacheImplementation<Loadable<T>> = treeCacheFromPolicy(
  cachePolicy ?? {
    equality: 'reference',
    eviction: 'keep-all',
  },
  key,
);

// 设置 selector 的保留策略
const retainedBy = retainedByOptionWithDefault(options.retainedBy_UNSTABLE);

// executionInfoMap 是一个 map 类型的数据结构，主要用于存储每个 store 的执行信息
const executionInfoMap: Map<Store, ExecutionInfo<T>> = new Map();
// 这是一个计数器，用于记录当前活跃的 store 的数量
let liveStoresCount = 0;
```

总结一下，这段代码的作用是根据提供的 options 创建与该 selector 相关的各个数据结构，包括缓存、依赖项集合、执行信息映射之类的。



接下来我们来看一下返回值。对应的代码如下：

```ts
if (set != null) {
    /**
     * ES5 strict mode prohibits defining non-top-level function declarations,
     * so don't use function declaration syntax here
     */
    const selectorSet = (
      store: Store,
      state: TreeState,
      newValue: T | DefaultValue,
    ): AtomWrites => {
      // ...
    };

    return (recoilValue = registerNode<T>({
      key,
      nodeType: 'selector',
      peek: selectorPeek,
      get: selectorGet,
      set: selectorSet,
      init: selectorInit,
      invalidate: invalidateSelector,
      clearCache: clearSelectorCache,
      shouldDeleteConfigOnRelease: selectorShouldDeleteConfigOnRelease,
      dangerouslyAllowMutability: options.dangerouslyAllowMutability,
      shouldRestoreFromSnapshots: false,
      retainedBy,
    }));
  } else {
    return (recoilValue = registerNode<T>({
      key,
      nodeType: 'selector',
      peek: selectorPeek,
      get: selectorGet,
      init: selectorInit,
      invalidate: invalidateSelector,
      clearCache: clearSelectorCache,
      shouldDeleteConfigOnRelease: selectorShouldDeleteConfigOnRelease,
      dangerouslyAllowMutability: options.dangerouslyAllowMutability,
      shouldRestoreFromSnapshots: false,
      retainedBy,
    }));
  }
```

主要就是判断是否有 set，不管有没有 set，最终调用的都是 registerNode 方法。nodeType 对应的为 selector。

如果 set 不为 null，那么在调用 registerNode 方法的时候，会设置 set 对应 selectorSet，说明会创建一个可读可写类型的 selector。

如果 set 为 null，那么会创建一个只读类型的 selector。



接下来看一下 selector 里面对应的内部方法，这里主要看一下：

- selectorPeek
- selectorGet
- selectorSet
- selectorInit
- invalidateSelector

