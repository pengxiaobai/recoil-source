# Atom源码解析

回顾之前学习的 Recoil，整个 Recoil 有两个核心概念：

- atom
- selector

还有三个常用的 Hook：

- useRecoilState
- useRecoilValue
- useSetRecoilState

这门课我们主要也就是针对两个核心概念以及三个 Hook 进行一个源码解析。

因为整个 Recoil 的源码量很多，而且源码本身是使用 Flow.js 来编写的，整体源码的可阅读性其实比较差的，所以我们即便是针对 atom 和 selector 核心概念进行源码解析，也只对里面几个比较核心的方法进行拆解。



首先回顾一下 Atom 的用法：

```ts
atom({
  key: 'xxx',
  default: xxx
})
```



## atom方法

atom 方法的源码如下：

```ts
function atom<T>(options: AtomOptions<T>): RecoilState<T> {
  // 将 options 对象里面的所有属性复制到 restOptions 里面
  const {...restOptions} = options;
  // 检查 options 里面是否有默认值，如果有默认值，就将默认值赋值给 optionsDefault
  // 否则的话，optionsDefault 是一个新的未决的 Promise
  const optionsDefault: RecoilValue<T> | Promise<T> | Loadable<T> | WrappedValue<T> | T =
    'default' in options
      ?
        options.default
      : new Promise(() => {});

  // 接下来判断 optionsDefault 是否是一个 RecoilValue 的值
  if (isRecoilValue(optionsDefault)
  ) {
    // 如果是 RecoilValue，进入此分支，调用 atomWithFallback 方法
    return atomWithFallback<T>({
      ...restOptions,
      default: optionsDefault,
    });
  } else {
    // 否则的话，调用 baseAtom 方法
    return baseAtom<T>({...restOptions, default: optionsDefault});
  }
}
```

atom 方法返回一个 RecoilState 类型的值。对应的类型相关信息的源码如下：

```ts
class AbstractRecoilValue<+T> {
  key: NodeKey;
  constructor(newKey: NodeKey) {
    this.key = newKey;
  }
  toJSON(): {key: string} {
    return {key: this.key};
  }
}

class RecoilState<T> extends AbstractRecoilValue<T> {}

class RecoilValueReadOnly<+T> extends AbstractRecoilValue<T> {}

export type RecoilValue<T> = RecoilValueReadOnly<T> | RecoilState<T>;
```

RecoilState 和 RecoilValueReadOnly 都继承了 AbstractRecoilValue。

还有一个类型 RecoilValue，该类型就是 RecoilState 和 RecoilValueReadOnly 的联合类型。



## atomWithFallback 方法

atomWithFallback 方法的源码如下：

```ts
function atomWithFallback<T>(options: AtomWithFallbackOptions<T>,): RecoilState<T> {
  // 整个 atomWithFallback 的核心逻辑可以分为两大块
  
  // 第一部分：再次调用 atom 方法
  // 这里再次调用 atom 方法会不会形成一个无限递归 ？
  // 答案是不会，因为第二次调用 atom 方法的时候，default 设置为了 DEFAULT_VALUE
  // 最终 base 对应的值其实就是 baseAtom 的返回值
  const base = atom<T | DefaultValue>({
    ...options,
    default: DEFAULT_VALUE,
    persistence_UNSTABLE:
      options.persistence_UNSTABLE === undefined
        ? undefined
        : {
            ...options.persistence_UNSTABLE,
            validator: (storedValue: mixed) =>
              storedValue instanceof DefaultValue
                ? storedValue
                : nullthrows(options.persistence_UNSTABLE).validator(
                    storedValue,
                    DEFAULT_VALUE,
                  ),
          },
    effects: (options.effects: any),
    effects_UNSTABLE: (options.effects_UNSTABLE: any),
  });

  // 第二部分：根据上一步所得到的 baseAtom，创建一个 selector，并返回外部
  const sel = selector<T>({
    key: `${options.key}__withFallback`,
    get: ({get}) => {
      const baseValue = get(base);
      return baseValue instanceof DefaultValue ? options.default : baseValue;
    },
    set: ({set}, newValue) => set(base, newValue),
    cachePolicy_UNSTABLE: {
      eviction: 'most-recent',
    },
    dangerouslyAllowMutability: options.dangerouslyAllowMutability,
  });
  setConfigDeletionHandler(sel.key, getConfigDeletionHandler(options.key));
  return sel;
}
```



## baseAtom方法

首先看一下 baseAtom 方法最终返回的产物：

```ts
const node = registerNode(
  (
    {
    key,
    nodeType: 'atom',
    peek: peekAtom,
    get: getAtom,
    set: setAtom,
    init: initAtom,
    invalidate: invalidateAtom,
    shouldDeleteConfigOnRelease: shouldDeleteConfigOnReleaseAtom,
    dangerouslyAllowMutability: options.dangerouslyAllowMutability,
    persistence_UNSTABLE: options.persistence_UNSTABLE
      ? {
          type: options.persistence_UNSTABLE.type,
          backButton: options.persistence_UNSTABLE.backButton,
        }
      : undefined,
    shouldRestoreFromSnapshots: true,
    retainedBy,
  }: ReadWriteNodeOptions<T>),
);
return node;
```

可以看到，这里会调用 registerNode 的方法，该方法会有一个返回值，最终外部拿到的其实是 registerNode 方法的返回值。registerNode 方法在调用的时候接收一个配置对象，该配置对象有一些如下的重要字段：

- key：全局内的一个唯一标识，用来标识 atom。
- nodeType：表示这是一个 atom 类型的节点
- 有一些重要的方法：peek、get、set、init、invalidate，peekAtom 是用来预览 atom 的值，getAtom 用来获取 atom 的值，setAtom 用来设置 atom 的值，initAtom 是用来初始化 atom 的值，invalidateAtom 让一个 atom 无效。

registerNode 的源码如下：

```ts
const nodes: Map<string, Node<any>> = new Map();
const recoilValues: Map<string, RecoilValue<any>> = new Map();

function registerNode<T>(node: Node<T>): RecoilValue<T> {
  if (RecoilEnv.RECOIL_DUPLICATE_ATOM_KEY_CHECKING_ENABLED) {
    checkForDuplicateAtomKey(node.key);
  }
  
  // 这一步是将传入的对象（node）存储到 nodes 的 map 里面
  nodes.set(node.key, node);

  const recoilValue: RecoilValue<T> =
    // 判断传入的 node 对象是否有 set 方法
    // 如果没有 set 方法，那就是一个只读类型的数据，new 了一个 RecoilValueReadOnly
    // 如果有 set 方法，那么就是一个可读可写的数据，new 了一个 RecoilState
    node.set == null
      ? new RecoilValueClasses.RecoilValueReadOnly(node.key)
      : new RecoilValueClasses.RecoilState(node.key);

  // 将上一步生成的 recoilValue 存储到 recoilValues 的 map 里面
  recoilValues.set(node.key, recoilValue);
  return recoilValue;
}
```



接下来我们来看一下在调用 registerNode，传入的对象有几个重要的方法：

- peekAtom
- getAtom
- setAtom
- initAtom



### initAtom方法

该方法是内部一个比较核心的方法，该方法从名字我们就能看出是对 atom 做初始化操作的。

initAtom 方法的部分源码如下：

```ts
function initAtom(
    store: Store,
    initState: TreeState,
    trigger: Trigger,
  ): () => void {
    // 增加获取仓库的数量，这是 Recoil 中所使用到的一种技术，确保当所有的 store 都不再使用某个 atom 时，可以执行一些清理操作
    liveStoresCount++;
    
    // 这就是一个清理函数，该函数内部定义了当 atom 不再被任何 store 使用时要执行的清理逻辑。
    const cleanupAtom = () => {
      liveStoresCount--;
      cleanupEffectsByStore.get(store)?.forEach(cleanup => cleanup());
      cleanupEffectsByStore.delete(store);
    };

    // 这一行代码是将 atom 的 key 添加到 store 内部的 atom keys 列表里面。
    // 这一步是非常有必要的，因为 Recoil 需要追踪哪些 atom 是已经被初始化并且正在被 store 使用的。
    store.getState().knownAtoms.add(key);

    // 如果 defaultLoadable 的 state 状态为 loading
    // 设置一个函数 notifyDefaultSubscribers，用于当异步加载默认值完成时通知所有的订阅者
    if (defaultLoadable.state === 'loading') {
      const notifyDefaultSubscribers = () => {
        const state = store.getState().nextTree ?? store.getState().currentTree;
        if (!state.atomValues.has(key)) {
          markRecoilValueModified(store, node);
        }
      };
      defaultLoadable.contents.finally(notifyDefaultSubscribers);
    }
      
    // 之后是和 effect 副作用相关的代码，这一块省略...
      
    return cleanupAtom;
}
```

总结一下，这段代码主要的作用就是初始化一个 atom，并且设置相应的清理逻辑和异步加载的默认值。至于 initAtom 方法的后半段代码，是对 atom effect 副作用做出处理，这一段代码较长，并且前面我们介绍 Recoil 实战部分的时候也没有涉及 effect 副作用的使用，所以这里略过。



### getAtom方法

该方法从名字就可以看出，获取一个 atom 的当前值。

对应的源码如下：

```ts
function getAtom(_store: Store, state: TreeState): Loadable<T> {
  if (state.atomValues.has(key)) {
    // 首先看一下 state.atomValues 里面是否有这个 atom 值
    // 如果有，那么就直接返回这个值。
    // state.atomValues 是一个 map，它的键是 atom 的 key，值对应就是 atom 值
    return nullthrows(state.atomValues.get(key));
  } else if (state.nonvalidatedAtoms.has(key)) {
    // 如果在 state.nonvalidatedAtoms 里面存在 atom 这个值，说明 atom 的值是需要验证的
    // 因此该分支所做的事情，就是先对对应的 atom 的值进行一个验证，而且所验证后的结果是有可能被缓存的，方便后续直接使用
    
    if (cachedAnswerForUnvalidatedValue != null) {
      return cachedAnswerForUnvalidatedValue;
    }

    if (persistence == null) {
      expectationViolation(
        `Tried to restore a persisted value for atom ${key} but it has no persistence settings.`,
      );
      return defaultLoadable;
    }
    const nonvalidatedValue = state.nonvalidatedAtoms.get(key);
    const validatorResult: T | DefaultValue = persistence.validator(
      nonvalidatedValue,
      DEFAULT_VALUE,
    );

    const validatedValueLoadable =
      validatorResult instanceof DefaultValue
        ? defaultLoadable
        : loadableWithValue(validatorResult);

    cachedAnswerForUnvalidatedValue = validatedValueLoadable;

    return cachedAnswerForUnvalidatedValue;
  } else {
    // 进入此分支，说明 atom 的值不存在
    // 那么就返回一个默认值 defaultLoadable
    return defaultLoadable;
  }
}
```

总结一下，getAtom 方法主要作用就是获取一个 atom 的值，如果这个值不存在，那么就返回一个默认值（defaultLoadable），如果这个值存在，但是还没有经过验证，那么会执行验证的相关逻辑，然后返回验证的结果。



### setAtom方法

setAtom 对应的是设置 atom 的值：

```ts
function setAtom(
  _store: Store,
  state: TreeState,
  newValue: T | DefaultValue,
): AtomWrites {
  if (state.atomValues.has(key)) {
    // 判断是否有这个 atom
    const existing = nullthrows(state.atomValues.get(key));
    
    // 判断找到的 atom 的这个值和新值是否相同，如果相同，返回一个新的 map
    if (existing.state === 'hasValue' && newValue === existing.contents) {
      return new Map();
    }
  } else if (
    !state.nonvalidatedAtoms.has(key) &&
    newValue instanceof DefaultValue
  ) {
    
    // 还有一种情况也会返回一个新的 map，就是新的值是一个默认值的时候
    
    return new Map();
  }

  maybeFreezeValueOrPromise(newValue);

  cachedAnswerForUnvalidatedValue = undefined; 

  // 设置新的值
  // 创建一个新的 map，设置键为 atom 的可以，值为一个 loadable 类型的值
  return new Map<NodeKey, Loadable<$FlowFixMe>>().set(
    key,
    loadableWithValue(newValue),
  );
}
```

总结一下，setAtom 作用就是设置一个 atom 的新值，但是在设置之前，首先需要做一些判断，判断新值和旧值是否相同，又或者新的值是否为默认值，如果是，那么就返回一个空的 map，否则就设置新值，返回一个包含了新值的 map。



### peekAtom方法

用于预览当前的 atom 的值。

```ts
function peekAtom(_store: Store, state: TreeState): Loadable<T> {
  return (
    state.atomValues.get(key) ??
    cachedAnswerForUnvalidatedValue ??
    defaultLoadable
  );
}
```

内部就是通过 state.atomValues.get 方法去获取对应的 atom 的值返回给外部。如果没有就去缓存找，缓存也找不到就返回 defaultLoadable 默认值。

