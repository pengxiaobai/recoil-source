import styles from "../css/TodoListFilter.module.css";

// import { useRecoilState } from "recoil";
import { useRecoilState } from "../recoil/hooks";
import { todoListFilterState } from "../store";

function TodoListFilter() {
  // 首先获取 select 的状态值
  const [filter, setFilter] = useRecoilState(todoListFilterState);

  const updateFilter = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setFilter(e.target.value);
  };

  return (
    <select className={styles.container} value={filter} onChange={updateFilter}>
      <option value="all">所有项目</option>
      <option value="completed">已完成</option>
      <option value="uncompleted">未完成</option>
    </select>
  );
}

export default TodoListFilter;

