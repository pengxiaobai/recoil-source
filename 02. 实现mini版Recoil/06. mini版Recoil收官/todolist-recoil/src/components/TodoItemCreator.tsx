import { useState } from "react";
import { todoListState } from "../store";
// import { useRecoilState } from "recoil";
import { useRecoilState } from "../recoil/hooks";

import styles from "../css/TodoItemCreator.module.css";

function TodoItemCreator() {
  // 在内部维护一个状态，该状态用于输入框的值
  const [inputValue, setInputValue] = useState("");

  // 获取所有的待办事项以及设置待办事项的方法
  const [todolist, setTodoList] = useRecoilState(todoListState);

  // 添加新的待办事项
  const addItemHandle = () => {
    // 这里面需要做一些判断
    // 如果输入框的值为空，就不添加
    if (!inputValue) {
      window.alert("请输入待办事项的内容！");
      return;
    }
    // 还需要判断新的待办事项是否已经存在，如果存在也不允许添加
    const isExist = todolist.find((it) => it.content === inputValue);
    if (isExist) {
      window.alert("该待办事项已经存在！");
      return;
    }
    // 说明可以添加
    // 这里之所以报错，是因为我们给设置状态的方法传递的是一个函数，而不是一个值
    // 而在我们自己实现的 mini 版 recoil 中，我们的 set 方法只接受一个值，而不接受函数
    // setTodoList((oldTodoList) => [
    //   ...oldTodoList,
    //   {
    //     time: Date.now(),
    //     content: inputValue,
    //     isCompleted: false,
    //   },
    // ]);

    // 这里我们修改为接收一个值，而非函数
    setTodoList([
      ...todolist,
      {
        time: Date.now(),
        content: inputValue,
        isCompleted: false,
      },
    ]);

    setInputValue("");
  };

  return (
    <>
      <input
        type="text"
        className={styles.input}
        placeholder="請輸入待辦事項"
        value={inputValue}
        onChange={({ target }) => setInputValue(target.value)}
      />
      <button onClick={addItemHandle}>新增</button>
    </>
  );
}

export default TodoItemCreator;

