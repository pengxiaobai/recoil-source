import TodoItemCreator from "./TodoItemCreator";
import TodoListStatus from "./TodoListStatus";
import TodoItem from "./TodoItem";
import TodoListFilter from "./TodoListFilter";

import styles from "../css/TodoList.module.css";

// import { useRecoilValue } from "recoil";
import { useRecoilValue } from "../recoil/hooks";
import { todoListFilteredItem } from "../store";

export default function TodoList() {
  // 从数据仓库获取状态值
  const todolist = useRecoilValue(todoListFilteredItem);

  return (
    <>
      {/* 上面部分：创建待办事项 */}
      <TodoItemCreator />
      {/* 下面部分：分为左右两个部分，一个是项目列表，另外一个是项目状态 */}
      <div className={styles.container}>
        {/* 左边部分：项目列表 */}
        <div className={styles.leftArea}>
          <h4>项目列表</h4>
          {todolist.map((it, index) => (
            <TodoItem item={it} key={index} />
          ))}
          <TodoListFilter />
        </div>
        {/* 右边部分：项目状态 */}
        <div className={styles.rightArea}>
          <h4>项目状态</h4>
          <TodoListStatus />
        </div>
      </div>
    </>
  );
}

