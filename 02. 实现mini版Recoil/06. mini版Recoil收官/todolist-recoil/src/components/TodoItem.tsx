import { useState } from "react";
// import { useRecoilState } from "recoil";
import { useRecoilState } from "../recoil/hooks";
import { todoListState } from "../store";
import styles from "../css/TodoItem.module.css";

interface Item {
  content: string;
  isCompleted: boolean;
  time: string;
}

interface ItemProps {
  item: Item;
}

function TodoItem({ item }: ItemProps) {
  // 在该组件内部维护一个状态，该状态用于控制当前是否处于编辑模式
  const [editable, setEditable] = useState(false);

  // 再维护一个数据状态，用于编辑框对应的内容
  const [editInputValue, setEditInputValue] = useState("");

  // 获取待办事项列表状态
  const [todolist, setTodoList] = useRecoilState(todoListState);

  // 寻找对应项目的下标
  // 接收一个参数，参数是待办事项的内容
  const findItemIndex = (content: string) => {
    return todolist.findIndex((it) => it.content === content);
  };

  // 删除按钮对应的回调
  const closeHandler = (item: Item) => {
    // 询问用户是否要删除当前的待办事项
    if (
      window.confirm(
        `您确定要删除待办事项：${item.content} 吗？\n该项目当前的状态处于【${
          item.isCompleted ? "完成" : "未完成"
        }】`
      )
    ) {
      // 删除的逻辑
      const index = findItemIndex(item.content);

      if (index === -1) {
        return;
      }

      // 删除
      // 首先构建一个新的状态数组
      const newList = [
        ...todolist.slice(0, index),
        ...todolist.slice(index + 1),
      ];

      // 更新状态
      setTodoList(newList);
    }
  };

  // 修改待办事项的完成状态
  const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    // 先寻找对应的 item 的下标
    const index = findItemIndex(e.target.value);

    if (index === -1) {
      return;
    }

    // 进行完成状态的修改操作
    const editItem = Object.assign({}, todolist[index]);
    editItem.isCompleted = !editItem.isCompleted; // 修改当前项目的完成状态
    // 这一步就是将中间修改了完成状态的那一项进行了修改
    const newList = [
      ...todolist.slice(0, index),
      editItem,
      ...todolist.slice(index + 1),
    ];
    // 更新状态
    setTodoList(newList);
  };

  // 确认提交编辑内容
  const confirmEditContent = (item: Item) => {
    // 这里需要进行一些判断
    // 首先判断内容是否为空
    if (editInputValue.trim() === "") {
      window.alert("新的内容不能为空");
      return;
    }
    // 还需要判断新的编辑内容是否已经存在
    if (
      todolist.filter(
        (it) => it.content === editInputValue && it.content !== item.content
      ).length > 0
    ) {
      window.alert("该待办事项已经存在，请重新编辑");
      return;
    }

    // 询问用户是否要修改当前的待办事项
    if (
      item.content !== editInputValue &&
      window.confirm(
        `当前项目的内容为【${item.content}】？\n是否要修改为 【${editInputValue}】？`
      )
    ) {
      // 修改的逻辑
      const index = findItemIndex(item.content);
      if (index === -1) {
        return;
      }

      // 修改
      // 首先构建一个新的状态数组
      const newList = [
        ...todolist.slice(0, index),
        {
          ...item,
          content: editInputValue,
          // 需要将格式化好的时间重新转为时间戳
          time: new Date(item.time).getTime(),
        },
        ...todolist.slice(index + 1),
      ];

      // 更新状态
      setTodoList(newList);
    }
    // 关闭编辑模式
    setEditable(false);
  };

  return (
    <div className={styles.item}>
      {/* 在这里就需要根据 editable 的值来决定是渲染编辑框还是普通文本 */}

      <div>
        {editable ? (
          <span>
            <input
              type="text"
              className={styles.input}
              value={editInputValue}
              onChange={(e) => setEditInputValue(e.target.value)}
            />
            <span
              className={styles.confirmBtn}
              onClick={() => confirmEditContent(item)}
            >
              ✅
            </span>
          </span>
        ) : (
          <div className={styles.contentContainer}>
            <span>
              {/* 复选框 */}
              <input
                type="checkbox"
                className={styles.checkbox}
                checked={item.isCompleted}
                onChange={changeHandler}
                value={item.content}
              />
              {/* 文本内容 */}
              <span
                className={`${styles.pointer} ${
                  item.isCompleted ? styles.del : ""
                }`}
                onClick={() => {
                  // 打开编辑模式
                  setEditable(true);
                  // 将编辑框的内容设置为当前待办事项的内容
                  setEditInputValue(item.content);
                }}
              >
                {item.content}
              </span>
              {/* 发布时间 */}
              <span className={styles.time}>{item.time}</span>
            </span>
            {/* 删除按钮 */}
            <span className={styles.close} onClick={() => closeHandler(item)}>
              X
            </span>
          </div>
        )}
      </div>
    </div>
  );
}

export default TodoItem;

