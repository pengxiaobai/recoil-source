// import { useRecoilValue } from "recoil";
import { useRecoilValue } from "../recoil/hooks";
import { todoListStatus } from "../store";

import styles from "../css/TodoListStatus.module.css";

function TodoListStatus() {
  // 获取计算后的状态值
  const { totalNum, completedNum, uncompletedNum, percentCompleted, allText } =
    useRecoilValue(todoListStatus);

  return (
    <ul>
      <li>总项目数：{totalNum}</li>
      <li>完成项目数：{completedNum}</li>
      <li>未完成项目数：{uncompletedNum}</li>
      <li>完成百分比：{percentCompleted}%</li>
      <li className={styles.uncompletdTitle}>未完成项目：</li>
      {allText.length
        ? allText.map((it, index) => {
            return (
              <div key={index} className={styles.uncompletedItem}>
                {index + 1}. {it}
              </div>
            );
          })
        : "暂无未完成项目"}
    </ul>
  );
}

export default TodoListStatus;

