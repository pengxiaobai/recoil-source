import "./App.css";
import TodoList from "./components/TodoList";

function App() {
  return (
    <>
      <h1>待办事项2</h1>
      <p>这是一个熟悉 Recoil 基础使用的示例</p>
      <TodoList />
    </>
  );
}

export default App;

