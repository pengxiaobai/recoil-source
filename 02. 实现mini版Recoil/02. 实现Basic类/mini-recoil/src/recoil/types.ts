// 专门用来存放类型
export type voidFn = <T>(value: T) => void;
export type subscribeReturn = {
  unsubscribe: VoidFunction;
};

